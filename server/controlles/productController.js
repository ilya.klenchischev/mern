import asyncHandler from 'express-async-handler';
import Product from '../models/productModel.js';

// @description Fetch all products
// @route       GET /api/products
// @access      Public

const getProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({});
  res.json(products);
});

// @description Fetch single product
// @route       GET /api/products/:id
// @access      Public

const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    res.json(product);
  } else {
    res.status(404);
    throw new Error('Product not Found');
  }
});

// @description Delete a product
// @route       GET /api/products/:id
// @access      Prived/Admin

const deleteProduct = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    await product.remove();
    res.json({ message: 'Продукт был удален' });
  } else {
    res.status(404);
    throw new Error('Продукт не найден');
  }
});

// @description Create a product
// @route       POST /api/products
// @access      Prived/Admin

const createProduct = asyncHandler(async (req, res) => {
  const product = new Product({
    name: 'Sample name',
    price: 0,
    user: req.user._id,
    image: ['/images/sample.jpg'],
    brand: 'Sample brand',
    category: 'Sample category',
    countInStock: 0,
    numReviews: 0,
    description: 'Sample description',
    color: ['Sample color'],
    cloth: ['Sample cloth'],
    size: ['Sample size'],
    timer: false,
  });

  const createdProduct = await product.save();
  res.status(201).json(createdProduct);
});

// @description Update a product
// @route       PUT /api/products/:id
// @access      Prived/Admin

const updateProduct = asyncHandler(async (req, res) => {
  const {
    name,
    price,
    user,
    image,
    brand,
    category,
    countInStock,
    numReviews,
    description,
    color,
    cloth,
    size,
    timer,
  } = req.body;

  const product = await Product.findById(req.params.id);

  if (product) {
    product.name = name;
    product.price = price;
    product.description = description;
    product.image = image;
    product.brand = brand;
    product.category = category;
    product.countInStock = countInStock;
    product.numReviews = numReviews;
    product.color = color;
    product.cloth = cloth;
    product.size = size;
    product.timer = timer;

    const updateProduct = await product.save();
    res.json(updateProduct);
  } else {
    res.status(404);
    throw new Error('Продукт не найден');
  }
});

export {
  getProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateProduct,
};
