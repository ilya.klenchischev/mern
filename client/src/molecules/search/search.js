import React, { useState } from 'react';
import * as IconsGo from 'react-icons/go';
import { SearchInput } from '../../atoms/textarea/textarea';

const Search = () => {
  const [search, searchChange] = useState(false);

  const handleSearchChange = () => {
    searchChange(!search);
  };

  return (
    <>
      <SearchInput
        className={search ? 'input-search is-active' : 'input-search'}
        onClick={handleSearchChange}
      />
      <IconsGo.GoSearch onClick={handleSearchChange} className='search-icon' />
    </>
  );
};

export default Search;
