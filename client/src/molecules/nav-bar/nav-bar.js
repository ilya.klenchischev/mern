import React, { useRef } from 'react';
import { Link } from 'react-router-dom';

const NavBlock = (props) => {
  const navBlock = useRef();

  const handleMenuChange = () => {
    if ((window.innerWidth || document.documentElement.clientWidth) < 850) {
      if (navBlock.current) {
        if (!navBlock.current.classList.contains('is-active')) {
          navBlock.current.classList.add('is-active');
          navBlock.current.style.height = 'auto';

          let height = navBlock.current.clientHeight + 'px';
          navBlock.current.style.height = '0px';

          setTimeout(function () {
            navBlock.current.style.height = height;
          }, 0);
        } else {
          navBlock.current.style.height = '0px';
          navBlock.current.addEventListener(
            'transitionend',
            function () {
              navBlock.current.classList.remove('show');
            },
            {
              once: true,
            }
          );
        }
      }
    }
  };
  handleMenuChange();
  return (
    <>
      <nav ref={navBlock} className={props.className}>
        <ul className='nav-menu__menu' onClick={props.onClick}>
          <li className='nav-menu__text'>
            <Link to='/'>Главная</Link>
          </li>
          <li className='nav-menu__text'>
            <Link to='/catalog'>Каталог</Link>
          </li>
          <li className='nav-menu__text'>
            <Link to='/'>Home</Link>
          </li>
          <li className='nav-menu__text'>
            <Link to='/'>Home</Link>
          </li>
          <li className='nav-menu__text'>
            <Link to='/'>Home</Link>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default NavBlock;
