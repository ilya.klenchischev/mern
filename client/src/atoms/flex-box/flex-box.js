import React from 'react';

const FlexBox = ({
  children,
  alCenter,
  justifyContent,
  alEnd,
  alStart,
  noWrap,
}) => {
  function handleClock() {
    switch (justifyContent) {
      case 'flex-start':
        return ' flex-start';
      case 'center':
        return ' center';
      case 'flex-end':
        return ' flex-end';
      default:
        return '';
    }
  }

  return (
    <div
      className={`flex${handleClock()}${alCenter ? ' align-center' : ''}${
        alEnd ? ' align-end' : ''
      }${alStart ? ' align-start' : ''}${noWrap ? 'no-wrap' : ''}`}
    >
      {children}
    </div>
  );
};

export default FlexBox;
