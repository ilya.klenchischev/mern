import React from 'react';
import { Link } from 'react-router-dom';
import * as IconsBs from 'react-icons/bs';

const TextLink = ({ children, color, to, style }) => {
  return (
    <Link
      style={style}
      to={to}
      className={`button-link${color === 'blue' ? ' cBlue' : ''}`}
    >
      {children}
      <IconsBs.BsChevronRight />
    </Link>
  );
};

export { TextLink };
