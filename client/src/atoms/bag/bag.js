import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as IconsBs from 'react-icons/bs';

const Bag = ({ to }) => {
  return (
    <Link to={to} className='bag'>
      <IconsBs.BsBag />
    </Link>
  );
};

Bag.propTypes = {
  to: PropTypes.string.isRequired,
};

export default Bag;
