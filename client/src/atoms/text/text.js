import React from 'react';

const Title = ({ children, textCenter }) => {
  return <h1 className={`title${textCenter ? ' tcenter' : ''}`}>{children}</h1>;
};

const SubHeadline = ({ children, textCenter }) => {
  return <p className={`text${textCenter ? ' tcenter' : ''}`}>{children}</p>;
};

export { Title, SubHeadline };
