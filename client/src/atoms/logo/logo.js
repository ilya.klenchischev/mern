import React from 'react'
import * as AiIcons from 'react-icons/ai'
import './logo.scss'

const Logo = () => {
  return (
    <>
        <a className="logotype"><AiIcons.AiFillApple /></a>
    </>
  )
}

export default Logo
