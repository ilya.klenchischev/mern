import React from 'react';
import PropTypes from 'prop-types';

const Loading = (props) => {
  return (
    <div className={props.className}>
      <h1 className='loader'>
        <span>З</span>
        <span>А</span>
        <span>Г</span>
        <span>Р</span>
        <span>У</span>
        <span>З</span>
        <span>К</span>
        <span>А</span>
      </h1>
    </div>
  );
};

Loading.defaultProps = {
  className: 'loader-box',
};

Loading.propTypes = {
  className: PropTypes.string.isRequired,
};

export default Loading;
