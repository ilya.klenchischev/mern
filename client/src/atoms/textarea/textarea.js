import React from 'react';
import * as IconsGo from 'react-icons/go';
import * as IconsGr from 'react-icons/gr';

export const SearchInput = (props) => {
  return (
    <div className={props.className}>
      <IconsGo.GoSearch className='input-search__icon' />
      <input type='text' placeholder='Ищи товары в aesthetic-shop.com.ua' />
      <button onClick={props.onClick} className='input-search__close'>
        <IconsGr.GrClose className='input-search__close-icon' />
      </button>
    </div>
  );
};
