import React from 'react';

const Image = ({ src, alt, xl, xs, center, style }) => {
  return (
    <img
      className={`image${xl ? ' xl' : ''}${xs ? ' xs' : ''}${
        center ? ' center' : ''
      }`}
      src={src}
      alt={alt}
      style={style}
    />
  );
};

export default Image;
