import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import Bag from '../../atoms/bag/bag';
import { Burger } from '../../atoms/burger/burger';
import Logo from '../../atoms/logo/logo';
import NavBlock from '../../molecules/nav-bar/nav-bar';
import Search from '../../molecules/search/search';
import { logout } from '../../actions/userActions';

const Header = () => {
  const dispatch = useDispatch();

  const [sidebar, setSidebar] = useState(false);
  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;
  console.log(userInfo);
  const logoutHandler = () => {
    dispatch(logout());
  };

  const showSidebar = () => {
    if ((window.innerWidth || document.documentElement.clientWidth) < 850) {
      setSidebar(!sidebar);
    }
  };

  return (
    <header className={sidebar ? 'header is-active' : 'header'}>
      <div className='header__container'>
        <div onClick={showSidebar} className='header__burger'>
          <Burger
            className={`hamburglar ${sidebar ? 'is-open' : 'is-close'}`}
          />
        </div>
        <Logo />
        <NavBlock
          onClick={showSidebar}
          className={sidebar ? 'nav-menu is-active' : 'nav-menu'}
        />
        <div className='header__end'>
          <Search />
          <Bag to='/cart' />
        </div>
        {userInfo ? (
          <NavDropdown title={userInfo.name} id='username'>
            <LinkContainer to='/profile'>
              <NavDropdown.Item>Profile</NavDropdown.Item>
            </LinkContainer>
            <NavDropdown.Item onClick={logoutHandler}>Logout</NavDropdown.Item>
          </NavDropdown>
        ) : (
          <p>Login</p>
        )}
        {userInfo && userInfo.isAdmin && (
          <NavDropdown title='Admin' id='adminmenu'>
            <LinkContainer to='/admin/userlist'>
              <NavDropdown.Item>Users</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to='/admin/productlist'>
              <NavDropdown.Item>Products</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to='/admin/orderlist'>
              <NavDropdown.Item>Orders</NavDropdown.Item>
            </LinkContainer>
          </NavDropdown>
        )}
      </div>
    </header>
  );
};

export default Header;
