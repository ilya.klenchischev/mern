import React from 'react';
import { Title, SubHeadline } from '../../../../atoms/text/text';
import Image from '../../../../atoms/image/image';
import { TextLink } from '../../../../atoms/link/link';
import FlexBox from '../../../../atoms/flex-box/flex-box';

const MainScreen = () => {
  return (
    <section className='main-screen'>
      <div className='container'>
        <Title textCenter>Iphone</Title>
        <SubHeadline textCenter subheadline>
          При заказе сегодня у вас скидка 43%
        </SubHeadline>
        <FlexBox justifyContent='center' alCenter>
          <TextLink to='/' style={{ margin: '0 10px' }}>
            Подробнее
          </TextLink>
          <TextLink to='/' style={{ margin: '0 10px', color: 'red' }}>
            Заказать
          </TextLink>
        </FlexBox>
        <Image
          center
          style={{ margin: '20px auto' }}
          src='/images/phone.jpg'
          alt='iphone'
          xs
        />
      </div>
    </section>
  );
};

export default MainScreen;
