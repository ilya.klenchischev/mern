import React from 'react';
import { Container } from 'react-bootstrap';
import MainScreen from './components/main-screen/main-screen';

const MainPage = () => {
  return (
    <main>
      <MainScreen />
    </main>
  );
};

export default MainPage;
