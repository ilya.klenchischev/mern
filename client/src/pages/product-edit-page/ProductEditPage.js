import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Message from '../../atoms/message/message';
import Loader from '../../atoms/loading/Loading';
import FormContainer from '../../molecules/form-container/FormContainer';
import {
  listProductDetails,
  updateProduct,
} from '../../actions/productActions';
import { PRODUCT_UPDATE_RESET } from '../../constans/productConstans';

const ProductEditPage = ({ match, history }) => {
  const productId = match.params.id;

  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState(['']);
  const [category, setCategory] = useState('');
  const [description, setDescription] = useState('');
  const [rating, setRating] = useState(0);
  const [sale, setSale] = useState(0);
  const [color, setColor] = useState([]);
  const [cloth, setCloth] = useState([]);
  const [size, setSize] = useState([]);
  const [timer, setTimer] = useState(false);

  const dispatch = useDispatch();

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;

  const productUpdate = useSelector((state) => state.productUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = productUpdate;

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET });
      history.push('/admin/productlist');
    } else {
      if (!product.name || product._id !== productId) {
        dispatch(listProductDetails(productId));
      } else {
        setName(product.name);
        setPrice(product.email);
        setImage(product.image);
        setCategory(product.category);
        setDescription(product.description);
        setRating(product.rating);
        setSale(product.sale);
        setColor(product.color);
        setCloth(product.cloth);
        setSize(product.size);
        setTimer(product.timer);
      }
    }
  }, [dispatch, history, productId, product, successUpdate]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      updateProduct({
        _id: productId,
        name,
        price,
        image,
        category,
        description,
        rating,
        sale,
        color,
        cloth,
        size,
        timer,
      })
    );
  };

  return (
    <>
      <Link to='/admin/productlist' className='btn btn-light my-3'>
        Go back
      </Link>
      <FormContainer>
        <h1>Редактирование Продукта</h1>
        {loadingUpdate && <Loader className='loader-box is-active' />}
        {errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
        {loading ? (
          <Loader className='loader-box is-active' />
        ) : error ? (
          <Message variant='danger'>{error}</Message>
        ) : (
          <>
            <Loader className='loader-box is-close' />
            <Form onSubmit={submitHandler}>
              <Form.Group controlId='name'>
                <Form.Label>Название продукта</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Введите название'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='image'>
                <Form.Label>Фотографии</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Введите URL фотографии'
                  value={image}
                  onChange={(e) => setImage(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='price'>
                <Form.Label>Цена</Form.Label>
                <Form.Control
                  type='number'
                  placeholder='Введите цену'
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='sale'>
                <Form.Label>Оригинальная цена</Form.Label>
                <Form.Control
                  type='number'
                  placeholder='Enter Sale'
                  value={sale}
                  onChange={(e) => setSale(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='category'>
                <Form.Label>Категория</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Название категории'
                  value={category}
                  onChange={(e) => setCategory(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='rating'>
                <Form.Label>Рейтинг продукта от 1 до 5</Form.Label>
                <Form.Control
                  type='number'
                  placeholder='Число от 1 до 5'
                  value={rating}
                  onChange={(e) => setRating(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='description'>
                <Form.Label>Описание товара</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Введите описание'
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='color'>
                <Form.Label>Добавьте цвета продукта</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Введите цвет'
                  value={color}
                  onChange={(e) => setColor(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='cloth'>
                <Form.Label>Добавьте ткань продукта</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Введите название ткани'
                  value={cloth}
                  onChange={(e) => setCloth(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='size'>
                <Form.Label>Добавьте размеры</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Введите размер'
                  value={size}
                  onChange={(e) => setSize(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='timer'>
                <Form.Check
                  type='checkbox'
                  label='Включить таймер?'
                  value={timer}
                  onChange={(e) => setTimer(e.target.checked)}
                ></Form.Check>
              </Form.Group>
              <Button type='submit' variant='primary'>
                Update
              </Button>
            </Form>
          </>
        )}
      </FormContainer>
    </>
  );
};

export default ProductEditPage;
