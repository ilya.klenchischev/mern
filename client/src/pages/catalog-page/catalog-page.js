import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { Col, Container, Row } from 'react-bootstrap';
import Product from '../../molecules/product/product';
import { listProducts } from '../../actions/productActions';
import Loading from '../../atoms/loading/Loading';
import Message from '../../atoms/message/message';

const Catalog = () => {
  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;

  useEffect(() => {
    dispatch(listProducts());
  }, [dispatch]);

  return (
    <>
      <Container>
        <h1>Catalog Page</h1>
        {loading ? (
          <Loading className='loader-box is-acitve'></Loading>
        ) : error ? (
          <Message variant='danger'>{error}</Message>
        ) : (
          <>
            <Loading className='loader-box is-close'></Loading>
            <Row>
              {products.map((product) => (
                <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
                  <Product product={product} />
                </Col>
              ))}
            </Row>
          </>
        )}
      </Container>
    </>
  );
};

export default Catalog;
