import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './organisms/header/header';
import Footer from './organisms/footer/footer';
import MainPage from './pages/main-page/main-page';
import Catalog from './pages/catalog-page/catalog-page';
import ProductPage from './pages/product-page/ProductPage';
import CartPage from './pages/cart/CartPage';
import LoginPage from './pages/login-page/LoginPage';
import RegisterPage from './pages/register-page/RegisterPage';
import ProfilePage from './pages/profile-page/ProfilePage';
import ShippingPage from './pages/shipping-page/ShippingPage';
import PaymentPage from './pages/payment-page/PaymentPage';
import PlaceOrderPage from './pages/place-order-page/PlaceOrderPage';
import OrderPage from './pages/order-page/OrderPage';
import UserListPage from './pages/user-list-page/UserListPage';
import UserEditPage from './pages/user-edit-page/UserEditPage';
import ProductListPage from './pages/product-list-page/ProductListPage';
import ProductEditPage from './pages/product-edit-page/ProductEditPage';

function App() {
  return (
    <Router>
      <Header />
      <Route path='/order/:id' component={OrderPage} />
      <Route path='/shipping' component={ShippingPage} />
      <Route path='/payment' component={PaymentPage} />
      <Route path='/placeorder' component={PlaceOrderPage} />
      <Route path='/login' component={LoginPage} />
      <Route path='/register' component={RegisterPage} />
      <Route path='/profile' component={ProfilePage} />
      <Route path='/product/:id' component={ProductPage} />
      <Route path='/catalog' component={Catalog} />
      <Route path='/cart/:id?' component={CartPage} />
      <Route path='/admin/userlist' component={UserListPage} />
      <Route path='/admin/user/:id/edit' component={UserEditPage} />
      <Route path='/admin/productlist' component={ProductListPage} />
      <Route path='/' component={MainPage} exact />
      <Footer />
    </Router>
  );
}

export default App;
